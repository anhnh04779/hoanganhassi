package com.example.asus.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.asignment.Database;
import com.example.asus.asignment.ListSinhVienActivity;
import com.example.asus.asignment.R;
import com.example.asus.asignment.Student_ManagementActivity;
import com.example.asus.model.SinhVien;

import java.util.List;

/**
 * Created by ASUS on 7/31/2017.
 */

public class SinhVienAdapter extends ArrayAdapter<SinhVien> {
    Database database ;

   ListSinhVienActivity context; @LayoutRes int resource;@NonNull List<SinhVien> objects;
    public SinhVienAdapter(@NonNull ListSinhVienActivity context, @LayoutRes int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
        database=new Database(this.context);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=this.context.getLayoutInflater();
        View row=inflater.inflate(this.resource,null);

        TextView txtHoTenSinhVien= (TextView) row.findViewById(R.id.txtHoTenSinhVien);
        TextView txtNgaySinhNhat= (TextView) row.findViewById(R.id.txtNgaySinhNhat);
        TextView txtTinhThanh= (TextView) row.findViewById(R.id.txtTinhThanh);
        TextView txtLopMa= (TextView) row.findViewById(R.id.txtLopMa);
        ImageView imgHinhCustom= (ImageView) row.findViewById(R.id.imgHinhCusTom);
        Button btnXoaSinhVien= (Button) row.findViewById(R.id.btnXoaSinhVien);
        Button btnSuaThongTin= (Button) row.findViewById(R.id.btnSuaThongTin);
        final SinhVien sv =  this.objects.get(position);

        txtNgaySinhNhat.setText(sv.getNgaysinh());
        txtHoTenSinhVien.setText(sv.getTen());
        txtTinhThanh.setText(sv.getQueQuan());
        txtLopMa.setText(sv.getMaLop());
        //chuyen buye vè imgaeview
        byte[]hinhAnh=sv.getHinh();
        Bitmap bitmap= BitmapFactory.decodeByteArray(hinhAnh,0,hinhAnh.length);
        imgHinhCustom.setImageBitmap(bitmap);
        btnXoaSinhVien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            context.DialogXoaCongViec(sv.getTen(),sv.getStt());


            }
        });
        btnSuaThongTin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.DialogSuaCongViec(sv.getTen(),sv.getNgaysinh(),sv.getQueQuan(),sv.getHinh(),sv.getMaLop(),position+1);
            }
        });
        return row;
    }
}
