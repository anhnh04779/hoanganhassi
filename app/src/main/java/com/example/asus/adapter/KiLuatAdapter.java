package com.example.asus.adapter;

import android.app.Activity;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.example.asus.asignment.R;
import com.example.asus.model.DoiTuongKhenThuong;
import com.example.asus.model.DoiTuongKiLuat;

import java.util.List;

/**
 * Created by ASUS on 7/30/2017.
 */

public class KiLuatAdapter extends ArrayAdapter<DoiTuongKiLuat> {
    Activity context; @LayoutRes
    int resource; @NonNull
    List<DoiTuongKiLuat> objects;
    public KiLuatAdapter(@NonNull Activity context, @LayoutRes int resource, @NonNull List<DoiTuongKiLuat> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=this.context.getLayoutInflater();
        View row=inflater.inflate(this.resource,null);
        EditText txtMaKL= (EditText) row.findViewById(R.id.txtMaKL);
        EditText txtTenKL= (EditText) row.findViewById(R.id.txtTenKL);
        EditText txtNoiDungKL= (EditText) row.findViewById(R.id.txtNoiDungKl);
        EditText txtHinhThucKiLuat= (EditText) row.findViewById(R.id.txtHinhThucKiLuat);
        DoiTuongKiLuat doiTuongKhenThuong= this.objects.get(position);
        txtMaKL.setText(doiTuongKhenThuong.getMa());
        txtTenKL.setText(doiTuongKhenThuong.getTen());
        txtNoiDungKL.setText(doiTuongKhenThuong.getNoiDung());
        txtHinhThucKiLuat.setText(doiTuongKhenThuong.getHinhthuckl());
        return row;
    }
}
