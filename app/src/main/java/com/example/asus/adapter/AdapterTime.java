package com.example.asus.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.asus.asignment.R;
import com.example.asus.model.LichHoc;
import com.example.asus.model.SinhVien;

import java.util.List;

/**
 * Created by ASUS on 7/31/2017.
 */

public class AdapterTime extends ArrayAdapter<LichHoc> {
    @NonNull Activity context; @LayoutRes int resource; @NonNull List<LichHoc> objects;
    public AdapterTime(@NonNull Activity context, @LayoutRes int resource, @NonNull List<LichHoc> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=this.context.getLayoutInflater();
        View row=inflater.inflate(this.resource,null);
        TextView tvlop= (TextView) row.findViewById(R.id.tvlop);
        TextView tvmon= (TextView) row.findViewById(R.id.tvMon);
        TextView tvNgayHoc= (TextView) row.findViewById(R.id.tvNgayHoc);
        TextView tvThoiGian= (TextView) row.findViewById(R.id.tvThoiGian);
        LichHoc lichHoc =  this.objects.get(position);
        tvlop.setText(lichHoc.getLopHoc());
        tvNgayHoc.setText(lichHoc.getNgay());
        tvmon.setText(lichHoc.getMonHoc());
        tvThoiGian.setText(lichHoc.getThoiGianBatDau()+"-"+lichHoc.getThoiGianKetThuc());
        return row;
    }
}
