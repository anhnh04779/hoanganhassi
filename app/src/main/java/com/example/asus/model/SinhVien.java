package com.example.asus.model;

import java.io.Serializable;

/**
 * Created by ASUS on 7/27/2017.
 */

public class SinhVien implements Serializable {
    private int stt;
    private String ten;
    private String ngaysinh;
    private String QueQuan;
    private String maLop;
    private byte[] Hinh;
    public SinhVien() {
    }

    public SinhVien(int stt, String ten, String ngaysinh,String QueQuan,String maLop,byte[] hinh) {
        this.stt = stt;
        this.ten = ten;
        this.ngaysinh = ngaysinh;
        this.QueQuan=QueQuan;
        this.maLop=maLop;
        this.Hinh=hinh;
    }

    public byte[] getHinh() {
        return Hinh;
    }

    public void setHinh(byte[] hinh) {
        Hinh = hinh;
    }

    public int getStt() {
        return stt;
    }

    public void setStt(int stt) {
        this.stt = stt;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getNgaysinh() {
        return ngaysinh;
    }

    public void setNgaysinh(String ngaysinh) {
        this.ngaysinh = ngaysinh;
    }

    public String getQueQuan() {
        return QueQuan;
    }

    public void setQueQuan(String queQuan) {
        QueQuan = queQuan;
    }

    public String getMaLop() {
        return maLop;
    }

    public void setMaLop(String maLop) {
        this.maLop = maLop;
    }

    @Override
    public String toString() {
        return this.stt+"\t"+"\t"+this.ten+"\t"+"\t"+"\t"+"\t"+this.ngaysinh+"\t"+"\t"+this.maLop
                +"\t"+"\t"+this.QueQuan;
    }
}
