package com.example.asus.asignment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.asus.adapter.AdapterTime;
import com.example.asus.model.LichHoc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ManHinh5Activity extends AppCompatActivity {
    TextView txtDate, txtTimeS, txtTimeF;
    ImageView btnDate, btnTimeS, btnTimeF;
    Button btnThemTime;
    EditText txtMon;
    Spinner spLopHoc;
    ArrayList<String> dsLop;
    ArrayAdapter<String> adapterLop;
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
    ListView lvSapXep;
    ArrayList<LichHoc>dsTime;
   AdapterTime adapterTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_man_hinh5);
        setTitle("Sắp Xếp Tiết Học");
        addControls();
        addEvent();
    }

    private void addEvent() {
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XuLyHienThiDate();
            }
        });
        btnTimeS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XuLyHienThiTimeS();
            }
        });
        btnTimeF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XuLyHienThiTimeF();
            }
        });
        btnThemTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(txtMon.length()>0){
                   LichHoc lichHoc=new LichHoc();
                   lichHoc.setLopHoc(spLopHoc.getSelectedItem().toString());
                   lichHoc.setMonHoc(txtMon.getText().toString());
                   lichHoc.setNgay(txtDate.getText().toString());
                   lichHoc.setThoiGianBatDau(txtTimeS.getText().toString());
                   lichHoc.setThoiGianKetThuc(txtTimeF.getText().toString());
                   dsTime.add(lichHoc);
                   adapterTime.notifyDataSetChanged();
                   txtMon.setText("");

               }else{
                   Toast.makeText(ManHinh5Activity.this, "Dữ liệu không được trống", Toast.LENGTH_SHORT).show();


                   txtMon.requestFocus();
                   return;
               }
            }
        });
    }

    private void XuLyHienThiTimeF() {
        TimePickerDialog.OnTimeSetListener CallBack=new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                calendar.set(Calendar.MINUTE,minute);
                txtTimeF.setText(sdf2.format(calendar.getTime()));
            }
        };
        TimePickerDialog timePickerDialog;
        timePickerDialog = new TimePickerDialog(ManHinh5Activity.this,CallBack,
                calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),true);
        timePickerDialog.show();
    }

    private void XuLyHienThiTimeS() {
        TimePickerDialog.OnTimeSetListener CallBack=new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                calendar.set(Calendar.MINUTE,minute);
                txtTimeS.setText(sdf2.format(calendar.getTime()));
            }
        };
        TimePickerDialog timePickerDialog;
        timePickerDialog = new TimePickerDialog(ManHinh5Activity.this,CallBack,
                calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),true);
        timePickerDialog.show();
    }

    private void XuLyHienThiDate() {
        DatePickerDialog.OnDateSetListener CallBack = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                txtDate.setText(sdf1.format(calendar.getTime()));

            }
        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(ManHinh5Activity.this,
                CallBack, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void addControls() {
        txtDate = (TextView) findViewById(R.id.txtDate);
        txtTimeS = (TextView) findViewById(R.id.txtTimeS);
        txtTimeF= (TextView) findViewById(R.id.txtTimeF);
        btnDate= (ImageView) findViewById(R.id.btnDate);
        btnTimeS= (ImageView) findViewById(R.id.btnTimeS);
        btnTimeF= (ImageView) findViewById(R.id.btnTimeF);
        btnThemTime= (Button) findViewById(R.id.btnThemTime);
        txtMon= (EditText) findViewById(R.id.txtMon);
        spLopHoc= (Spinner) findViewById(R.id.spLopHoc);
        dsLop=new ArrayList<>();
        dsLop.add("Mob 5001");
        dsLop.add("Mob 5002");
        dsLop.add("Mob 5003");
        dsLop.add("Mob 5004");
        dsLop.add("Mob 5005");
        dsLop.add("Mob 5006");
        adapterLop=new ArrayAdapter<String>(ManHinh5Activity.this,android.R.layout.simple_spinner_item,dsLop);
        adapterLop.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLopHoc.setAdapter(adapterLop);
        calendar=Calendar.getInstance();
        txtDate.setText(sdf1.format(calendar.getTime()));
        txtTimeS.setText(sdf2.format(calendar.getTime()));
        txtTimeF.setText(sdf2.format(calendar.getTime()));
        lvSapXep= (ListView) findViewById(R.id.lvSapXep);
        dsTime=new ArrayList<>();
        adapterTime=new AdapterTime(ManHinh5Activity.this,R.layout.item_time,dsTime);
        lvSapXep.setAdapter(adapterTime);
    }
}
