package com.example.asus.asignment;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.example.asus.model.Lop;
import com.example.asus.model.SinhVien;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by Fpoly on 8/8/2017.
 */

public class Database extends SQLiteOpenHelper {

    private static final int Database_version = 1;
    private static final String Database_name = "1234567891.db";
    public Database(Context context) {
        super(context, Database_name, null, Database_version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String Table_Batch = "create table IF NOT EXISTS Lop(id Integer Primary key, maLop varchar(50) not null,tenLop varchar(50) )";
        String Table_Student = "create table IF NOT EXISTS SinhVien(id Integer  primary key ,ten varchar(50),ngaySinh varchar(50),queQuan varchar(50),HinhAnh BLOB,maLop varchar(50),FOREIGN KEY (maLop) REFERENCES Lop(maLop) )";
        db.execSQL(Table_Batch);
        db.execSQL(Table_Student);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Lop");
        db.execSQL("SinhVien");
        onCreate(db);
    }
    public void addLop(Lop lop){
        SQLiteDatabase database=this.getWritableDatabase();
        ContentValues values=new ContentValues();

        values.put("maLop",lop.getMaLop());
        values.put("tenLop",lop.getTenLop());
        database.insert("Lop",null,values);

    }
    public  void Insert_SinhVien(String ten, String ngaysinh,String QueQuan,byte[] hinh, String maLop){
        SQLiteDatabase database=this.getWritableDatabase();
        String sql="INSERT INTO SinhVien VALUES(null,?,?,?,?,?)";
        SQLiteStatement statement=database.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1,ten);
        statement.bindString(2,ngaysinh);
        statement.bindString(3,QueQuan);
        statement.bindBlob(4,hinh);
        statement.bindString(5,maLop);
        statement.executeInsert();
//        ContentValues values=new ContentValues();
//
//        values.put("ten",sinhVien.getTen());
//        values.put("ngaySinh",sinhVien.getNgaysinh());
//        values.put("maLop",sinhVien.getMaLop());
//        values.put("queQuan",sinhVien.getQueQuan());
//        database.insert("SinhVien",null,values);
    }
//    public void Update_SinhVien(int id,String ten, String ngaysinh,String QueQuan,byte[] hinh, String maLop){
//        SQLiteDatabase database=this.getWritableDatabase();
//        String sql="Update SinhVien Set ten='"+ten+",ngaySinh='"+ngaysinh+",queQuan='"+QueQuan+",HinhAnh='"+hinh+",maLop='"+maLop+"Where ID=''"+id+"'''''";
//        SQLiteStatement statement=database.compileStatement(sql);
//        statement.clearBindings();
//        statement.bindString(1,ten);
//        statement.bindString(2,ngaysinh);
//        statement.bindString(3,QueQuan);
//        statement.bindBlob(4,hinh);
//        statement.bindString(5,maLop);
//    }
    public ArrayList<Lop> getAllLop(){
        ArrayList<Lop> list=new ArrayList<>();
        String select="select * from Lop";
        SQLiteDatabase database=this.getWritableDatabase();
        Cursor cursor=database.rawQuery(select,null);
        if(cursor.moveToFirst()){
            do {
                Lop lop=new Lop();
                lop.setStt(Integer.parseInt(cursor.getString(0)));
                lop.setMaLop(cursor.getString(1));
                lop.setTenLop(cursor.getString(2));
                list.add(lop);
            }while (cursor.moveToNext());

        }

        return list;
    }
    public ArrayList<SinhVien> getAllSinhVien(){
        ArrayList<SinhVien>list=new ArrayList<>();
        String select="select * from SinhVien";
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(select,null);
        if (cursor.moveToFirst()){
            do{
                SinhVien sinhVien=new SinhVien();
                sinhVien.setStt(Integer.parseInt(cursor.getString(0)));
                sinhVien.setTen(cursor.getString(1));
                sinhVien.setNgaysinh(cursor.getString(2));
                sinhVien.setMaLop(cursor.getString(5));
                sinhVien.setHinh(cursor.getBlob(4));
                sinhVien.setQueQuan(cursor.getString(3));
                list.add(sinhVien);
            }while (cursor.moveToNext());

        }
        return list;
    }
    Lop getLop(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("Lop", new String[] { "id",
                        "maLop", "tenLop" }, "id" + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Lop contact = new Lop(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return contact
        return contact;
    }
    public ArrayList<String>  getLop(){
        String s="";
        ArrayList<String> list=new ArrayList<>();
        String select="SELECT maLop FROM Lop ORDER by maLop ASC";
        SQLiteDatabase database=this.getWritableDatabase();
        Cursor cursor=database.rawQuery(select,null);
        if(cursor.moveToFirst()){
            do {
                Lop lop=new Lop();
                ;
                lop.setMaLop(cursor.getString(0));
                s=lop.getMaLop();
                list.add(s);
            }while (cursor.moveToNext());

        }

        return list;
    }
    public ArrayList<SinhVien> getMALop(String Malop){

        ArrayList<SinhVien> list=new ArrayList<>();
      String select="SELECT * FROM SinhVien WHERE maLop = '" + Malop + "'";
        SQLiteDatabase database=this.getWritableDatabase();
        Cursor cursor=database.rawQuery(select,null);
        if (cursor.moveToFirst()){
            do{
                SinhVien sinhVien=new SinhVien();
                sinhVien.setStt(Integer.parseInt(cursor.getString(0)));
                sinhVien.setTen(cursor.getString(1));
                sinhVien.setNgaysinh(cursor.getString(2));
                sinhVien.setMaLop(cursor.getString(5));
                sinhVien.setHinh(cursor.getBlob(4));
                sinhVien.setQueQuan(cursor.getString(3));
                list.add(sinhVien);
            }while (cursor.moveToNext());

        }
        return list;
    }
    public void deleteLop(int position) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("Lop", "id" + " = ?", new String[]{String.valueOf(position)});
        database.close();
    }
    public void deleteSinhVien(int position) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("SinhVien", "id" + " = ?", new String[]{String.valueOf(position)});
        database.close();
    }
    public int updateSinhVien(SinhVien sinhVien){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("id",sinhVien.getStt());
        values.put("ten",sinhVien.getTen());
        values.put("ngaySinh",sinhVien.getNgaysinh());
        values.put("queQuan",sinhVien.getQueQuan());
        values.put("HinhAnh",sinhVien.getHinh());
        values.put("maLop",sinhVien.getMaLop());
        return  db.update("SinhVien",values,"id"+"=?",new String[]{String.valueOf(sinhVien.getStt())});

    }
    public Cursor getData(String sql) {
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql, null);
    }
    public void QueryData(String sql) {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);
    }


}
