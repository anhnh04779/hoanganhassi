package com.example.asus.asignment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.asus.adapter.SinhVienAdapter;
import com.example.asus.model.SinhVien;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class ListSinhVienActivity extends AppCompatActivity {
    ListView lvSinhVien;
    ArrayList<SinhVien> dsSinhVien;
    SinhVienAdapter adaptersinhVien;
    Spinner spLopHoc;
    ArrayList<String>dsLop;
    ArrayAdapter<String>adapterLop;
    Database database;
    int i=0;

    int REQUEST_CODE_UPLOAD=456;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_sinh_vien);
        addControl();
        addEvent();
    }

    private void addEvent() {
    spLopHoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            String maLop=spLopHoc.getSelectedItem().toString();
            if(maLop.equals("Show all")){
                showAllSpinner();
            }else{
                showSpinter();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    });

    }

    private void showSpinter() {
        String maLop=spLopHoc.getSelectedItem().toString();
        dsSinhVien=database.getMALop(maLop);

        adaptersinhVien=new SinhVienAdapter(ListSinhVienActivity.this,R.layout.item_student,dsSinhVien);
        lvSinhVien.setAdapter(adaptersinhVien);
        adaptersinhVien.notifyDataSetChanged();
    }

    private void showAllSpinner() {
        String maLop=spLopHoc.getSelectedItem().toString();
       dsSinhVien=database.getAllSinhVien();

        adaptersinhVien=new SinhVienAdapter(ListSinhVienActivity.this,R.layout.item_student,dsSinhVien);
        lvSinhVien.setAdapter(adaptersinhVien);
        adaptersinhVien.notifyDataSetChanged();
    }


    private void addControl() {
                lvSinhVien = (ListView) findViewById(R.id.lvSinhVien);
        spLopHoc= (Spinner) findViewById(R.id.spLopHoc);
        dsSinhVien = new ArrayList<>();
        database=new Database(this);
        dsSinhVien=database.getAllSinhVien();
        adaptersinhVien=new SinhVienAdapter(ListSinhVienActivity.this,R.layout.item_student,dsSinhVien);
        lvSinhVien.setAdapter(adaptersinhVien);
        adaptersinhVien.notifyDataSetChanged();
        spLopHoc= (Spinner) findViewById(R.id.spLopHoc);
        dsLop = new ArrayList<>();

        dsLop=database.getLop();
        dsLop.add("Show all");
        adapterLop=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,dsLop);
        spLopHoc.setAdapter(adapterLop);

    }



    public void DialogXoaCongViec(final String tencv, final int id) {
        AlertDialog.Builder dialogXoa = new AlertDialog.Builder(this);
        dialogXoa.setMessage("Bạn có muốn xóa sinh viên : '" + tencv + "' không ? ");
        dialogXoa.setPositiveButton("Có ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               database.deleteSinhVien(id);
                Toast.makeText(ListSinhVienActivity.this, "Đã xóa " + tencv, Toast.LENGTH_SHORT).show();
                dsSinhVien=new ArrayList<SinhVien>();
                dsSinhVien=database.getAllSinhVien();
                adaptersinhVien=new SinhVienAdapter(ListSinhVienActivity.this,R.layout.item_student,dsSinhVien);
                lvSinhVien.setAdapter(adaptersinhVien);
                adaptersinhVien.notifyDataSetChanged();

            }
        });

        dialogXoa.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogXoa.show();
    }

public void DialogSuaCongViec(String ten, final String ngaysinh, final String QueQuan, final byte[] hinh, String maLop, final int id) {

    final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.dialog_update_student);

    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
    lp.copyFrom(dialog.getWindow().getAttributes());
    lp.width = WindowManager.LayoutParams.MATCH_PARENT;


    Button btnXacNhan = (Button) dialog.findViewById(R.id.btnXacNhan);
    Button btnHuy = (Button) dialog.findViewById(R.id.btnHuy);
    final ImageView imgCustomStudent= (ImageView) dialog.findViewById(R.id.imgCusTomStudent);
    final EditText edtName= (EditText) dialog.findViewById(R.id.edtName);
    final EditText edtBirthday= (EditText) dialog.findViewById(R.id.edtBirthday);
    final EditText edtHomeTown= (EditText) dialog.findViewById(R.id.edtHomeTown);
    final EditText edtClass= (EditText) dialog.findViewById(R.id.edtClass);
     ImageView imgupdate= (ImageView) dialog.findViewById(R.id.imgupdate);
    imgupdate.setOnClickListener(new View.OnClickListener() {
        @Override

        public void onClick(View view) {
            Intent intent=new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent,REQUEST_CODE_UPLOAD);



        }
    });







    edtName.setText(ten);
    edtBirthday.setText(ngaysinh);
    edtHomeTown.setText(QueQuan);
    edtClass.setText(maLop);

    btnXacNhan.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String ten=edtName.getText().toString();
            String quequan=  edtHomeTown.getText().toString();
            String ngaySinh=edtBirthday.getText().toString();
            String maLop=  edtClass.getText().toString();
            BitmapDrawable bitmapDrawable= (BitmapDrawable) imgCustomStudent.getDrawable();
            Bitmap bitmap=bitmapDrawable.getBitmap();
            ByteArrayOutputStream byteArray=new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArray);
            byte[] hinhAnh= byteArray.toByteArray();

          database.updateSinhVien(new SinhVien(id,ten,ngaySinh,quequan,maLop,hinhAnh));
//            database.QueryData("Update SinhVien Set ten='"+ten+"',ngaySinh='"+ngaySinh+"',queQuan='"+quequan+"'" + ",maLop='"+maLop+"'Where Id='"+id+"'");
            Toast.makeText(ListSinhVienActivity.this, "Đã cập nhật", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            dsSinhVien=database.getAllSinhVien();
            adaptersinhVien=new SinhVienAdapter(ListSinhVienActivity.this,R.layout.item_student,dsSinhVien);
            lvSinhVien.setAdapter(adaptersinhVien);
            adaptersinhVien.notifyDataSetChanged();


        }
    });

    btnHuy.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dialog.dismiss();
//                dialog.cancel();
        }
    });


    dialog.show();
    dialog.getWindow().setAttributes(lp);

}


}
