package com.example.asus.asignment;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.asus.adapter.LopAdapter;
import com.example.asus.model.Lop;

import java.util.ArrayList;

public class ListClassActivity extends AppCompatActivity {
    ListView lvLop;
    ArrayList<Lop> dsLop;
    LopAdapter adapterLop;
    Database database;
    int a = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_class);
        setTitle("Xem Danh Sách Lớp");

        addControls();
        addEvent();
    }

    private void addEvent() {
        lvLop.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ListClassActivity.this);
                builder.setTitle("Xóa");
                builder.setMessage("Bạn có muốn xóa lớp này không");
                builder.setCancelable(true);
                builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        database.deleteLop(dsLop.get(position).getStt());
                        dsLop = new ArrayList<>();
                        dsLop = database.getAllLop();
                        adapterLop=new LopAdapter(ListClassActivity.this,R.layout.itemlop,dsLop);

                        lvLop.setAdapter(adapterLop);

                    }
                });
                builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();



                return false;
            }
        });
    }

    private void addControls() {
        database = new Database(this);
        lvLop = (ListView) findViewById(R.id.lvLop);


//        dsLop.add( new Lop(1,"Mob5001","Lớp Mobile 5001"));
//        dsLop.add( new Lop(2,"Mob5002","Lớp Mobile 5002"));
//        dsLop.add( new Lop(3,"Mob5003","Lớp Mobile 5003"));
//        dsLop.toString();
        dsLop = new ArrayList<>();
        dsLop = database.getAllLop();
        adapterLop=new LopAdapter(ListClassActivity.this,R.layout.itemlop,dsLop);
        lvLop.setAdapter(adapterLop);
        adapterLop.notifyDataSetChanged();

    }

    public void showAlertDiaLog() {

    }
}
