package com.example.asus.asignment;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.asus.model.Lop;

import java.util.ArrayList;
import android.app.Activity;
import android.view.Window;
/**
 * Created by ASUS on 7/26/2017.
 */

public class DialogActivity extends Activity {
    Button btnxoa,btnLuuLop;
    EditText txtMa,txtTenLop;
    Database database;
    ArrayList<Lop>list=new ArrayList<>();
    int i=0;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Thêm Lớp Sinh Viên");
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout);
        database=new Database(this);

        addControls();
        addEvents();
    }

    private void addEvents() {
        btnxoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMa.setText("");
                txtTenLop.setText("");
            }
        });
        btnLuuLop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtMa.length()>0&&txtTenLop.length()>0) {

                    database.addLop(new Lop(i,txtMa.getText().toString(),txtTenLop.getText().toString()));



                    Toast.makeText(DialogActivity.this, "Lưu Lớp Thành Công", Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    Toast.makeText(DialogActivity.this, "Lưu Lớp Thất Bại", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

//

    private void addControls() {
        txtMa= (EditText) findViewById(R.id.txtMa);
        txtTenLop= (EditText) findViewById(R.id.txtTenLop);
        btnxoa= (Button) findViewById(R.id.btnXoa);
        btnLuuLop= (Button) findViewById(R.id.btnLuuLop);



    }


    }



