package com.example.asus.asignment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Student_ManagementActivity extends AppCompatActivity {
    EditText txtTen, txtNgaySinh, txtMaSinhVien;
    ImageView btnNgaySinh;
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
    Button btnThemSinhVien;
    Spinner spLop;
    ArrayList<String> dsLop;
    ArrayAdapter<String> adapterLop;
    ImageView imgupload,imgHinhSV;
//    ListView lvSinhVien;
//    ArrayList<SinhVien> dsSinhVien;
//    SinhVienAdapter adaptersinhVien;
    String[] arrTinhThanh;
    AutoCompleteTextView txtQueQuan;
    ArrayAdapter<String> adapterTinhThanh;
    int REQUEST_CODE_UPLOAD=456;
    int i = 0;
    public  static Database database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_management);
        setTitle("Quản Lý Sinh Viên");
        addControls();
        addEvent();
    }

    private void addControls() {
        database=new Database(this);
        txtTen = (EditText) findViewById(R.id.txtTen);
        txtNgaySinh = (EditText) findViewById(R.id.txtNgaySinh);
        btnThemSinhVien = (Button) findViewById(R.id.btnThemSinhVien);

        txtQueQuan = (AutoCompleteTextView) findViewById(R.id.txtQueQuan);
        spLop = (Spinner) findViewById(R.id.spLop);
        dsLop = new ArrayList<>();

        dsLop=database.getLop();

//        dsLop.add("Lớp Mobile 5001");
//        dsLop.add("Lớp Mobile 5002");
//        dsLop.add("Lớp Mobile 5003");
        adapterLop = new ArrayAdapter<String>(Student_ManagementActivity.this, android.R.layout.simple_spinner_item, dsLop);
        adapterLop.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLop.setAdapter(adapterLop);
//        lvSinhVien = (ListView) findViewById(R.id.lvSinhVien);
//        dsSinhVien = new ArrayList<>();
       btnNgaySinh= (ImageView) findViewById(R.id.btnNgaySinh);
//        adaptersinhVien = new SinhVienAdapter(Student_ManagementActivity.this, R.layout.item_student, dsSinhVien);
//        lvSinhVien.setAdapter(adaptersinhVien);
        arrTinhThanh = getResources().getStringArray(R.array.arrTinhThanh);
        adapterTinhThanh = new ArrayAdapter<String>(Student_ManagementActivity.this, android.R.layout.simple_list_item_1, arrTinhThanh);
        txtQueQuan.setAdapter(adapterTinhThanh);
        calendar=Calendar.getInstance();
        txtNgaySinh.setText(sdf1.format(calendar.getTime()));
        imgupload= (ImageView) findViewById(R.id.imgupload);
        imgHinhSV= (ImageView) findViewById(R.id.imgHinhSv);
    }

    private void addEvent() {
        btnThemSinhVien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtTen.length()>0&&txtNgaySinh.length()>0&&txtQueQuan.length()>0){
//                    i++;
//                    SinhVien sv = new SinhVien();
//                    sv.setStt(i);
//                    sv.setTen(txtTen.getText().toString());
//                    sv.setNgaysinh(txtNgaySinh.getText().toString());
//                    sv.setQueQuan(txtQueQuan.getText().toString());
//                    sv.setMaLop(spLop.getSelectedItem().toString());
//                    dsSinhVien.add(sv);
//                    adaptersinhVien.notifyDataSetChanged();
//                    txtTen.setText("");
//                    txtQueQuan.setText("");
//                    txtNgaySinh.setText("");
                    SaveSinhVien();
//                    dsSinhVien=new ArrayList<SinhVien>();
//                    dsSinhVien=database.getAllSinhVien();
//                    adaptersinhVien = new SinhVienAdapter(Student_ManagementActivity.this, R.layout.item_student, dsSinhVien);
//                    lvSinhVien.setAdapter(adaptersinhVien);
//                    adaptersinhVien.notifyDataSetChanged();
                }else{
                    Toast.makeText(Student_ManagementActivity.this, "Dữ Liêu không được rỗng", Toast.LENGTH_SHORT).show();
                    txtTen.requestFocus();txtQueQuan.requestFocus();
                    return;
                }





            }
        });
        btnNgaySinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XuLyHienThiNgaySinh();
            }
        });
        imgupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,REQUEST_CODE_UPLOAD);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==REQUEST_CODE_UPLOAD&&resultCode==RESULT_OK&&data!=null){
                Uri uri=data.getData();
                try {
                    InputStream inputStream=getContentResolver().openInputStream(uri);
                    Bitmap bitmap= BitmapFactory.decodeStream(inputStream);
                    imgHinhSV.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
        }
        super.onActivityResult(requestCode,resultCode,data);
    }
    private void SaveSinhVien() {
        String ten=txtTen.getText().toString();
        String quequan=txtQueQuan.getText().toString();
        String ngaySinh=txtNgaySinh.getText().toString();
        String maLop=  spLop.getSelectedItem().toString();
//        SinhVien sinhVien=new SinhVien(i,ten,ngaySinh,maLop,quequan);
        BitmapDrawable bitmapDrawable= (BitmapDrawable) imgHinhSV.getDrawable();
        Bitmap bitmap=bitmapDrawable.getBitmap();
        ByteArrayOutputStream byteArray=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArray);
        byte[] hinhAnh= byteArray.toByteArray();
        Student_ManagementActivity.database.Insert_SinhVien(ten,ngaySinh,quequan,hinhAnh,maLop);
    }
    private void XuLyHienThiNgaySinh() {
        DatePickerDialog.OnDateSetListener CallBack = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                txtNgaySinh.setText(sdf1.format(calendar.getTime())+"");

            }
        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(Student_ManagementActivity.this,
                CallBack, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
}
