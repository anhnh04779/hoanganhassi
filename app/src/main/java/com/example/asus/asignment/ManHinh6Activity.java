package com.example.asus.asignment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.example.asus.adapter.KhenThuongAdapter;
import com.example.asus.adapter.KiLuatAdapter;
import com.example.asus.model.DoiTuongKhenThuong;
import com.example.asus.model.DoiTuongKiLuat;

import java.util.ArrayList;

public class ManHinh6Activity extends AppCompatActivity {
    EditText txtNameKT,txtIDKT,txtNDKT,txtNameKL,txtIDKL,txtNDKL,txtHinhThucKL,txtHinhThucKhenThuong,txtMaKT,txtNoiDungKT,txtTenKT;
    Button btnXacNhanKhenThuong,btnXacNhanKiLuat;
    ListView lvKhenThuong,lvKiLuat;
    ArrayList<DoiTuongKhenThuong>dsDoiTuong;
    KhenThuongAdapter adapter;
    ArrayList<DoiTuongKiLuat> dsKiluat;
    KiLuatAdapter kiLuatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_man_hinh6);
        addControl();
        addEvent();
    }

    private void addEvent() {
        btnXacNhanKhenThuong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtIDKT.length()>0&&txtNameKT.length()>0&&txtNDKT.length()>0&&txtHinhThucKhenThuong.length()>0){
                    DoiTuongKhenThuong dt=new DoiTuongKhenThuong();
                    dt.setMa(txtIDKT.getText().toString());
                    dt.setTen(txtNameKT.getText().toString());
                    dt.setNoiDung(txtNDKT.getText().toString());
                    dt.setPhanThuong(txtHinhThucKhenThuong.getText().toString());
                    dsDoiTuong.add(dt);
                    adapter.notifyDataSetChanged();
                    txtIDKT.setText("");
                    txtNameKT.setText("");
                    txtNDKT.setText("");
                    txtHinhThucKhenThuong.setText("");
                }else{
                    Toast.makeText(ManHinh6Activity.this, "Dữ liệu không được trống", Toast.LENGTH_SHORT).show();
                }



            }
        });
        btnXacNhanKiLuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (txtIDKL.length()>0&&txtNameKL.length()>0&&txtNDKL.length()>0&&txtHinhThucKL.length()>0){
                DoiTuongKiLuat dt=new DoiTuongKiLuat();
                dt.setMa(txtIDKL.getText().toString());
                dt.setTen(txtNameKL.getText().toString());
                dt.setNoiDung(txtNDKL.getText().toString());
                dt.setHinhthuckl(txtHinhThucKL.getText().toString());
                dsKiluat.add(dt);
                kiLuatAdapter.notifyDataSetChanged();
                txtIDKL.setText("");
                txtNameKL.setText("");
                txtNDKL.setText("");
                txtHinhThucKL.setText("");
            }else{
                Toast.makeText(ManHinh6Activity.this, "Dữ liệu không được trống", Toast.LENGTH_SHORT).show();
            }

            }
        });
    }

    private void addControl() {
        TabHost tabHost= (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();
        TabHost.TabSpec tab1=tabHost.newTabSpec("t1");
        tab1.setContent(R.id.tab1);
        tab1.setIndicator("Khen Thưởng");
        tabHost.addTab(tab1);
        TabHost.TabSpec tab2=tabHost.newTabSpec("t2");
        tab2.setContent(R.id.tab2);
        tab2.setIndicator("Kỉ Luật");
        tabHost.addTab(tab2);
        txtNameKL= (EditText) findViewById(R.id.txtNamekl);
        txtNameKT= (EditText) findViewById(R.id.txtNameKT);
        txtIDKT= (EditText) findViewById(R.id.txtIDKT);
        txtNDKT= (EditText) findViewById(R.id.txtNDKT);
        txtIDKL= (EditText) findViewById(R.id.txtIDKL);
        txtHinhThucKL= (EditText) findViewById(R.id.txtHinhThucKL);
        txtNDKL= (EditText) findViewById(R.id.txtNDKL);
        txtMaKT= (EditText) findViewById(R.id.txtMaKT);
        txtHinhThucKhenThuong= (EditText) findViewById(R.id.txtHinhThucKhenThuong);
        txtNoiDungKT= (EditText) findViewById(R.id.txtNoiDungKT);
        txtTenKT= (EditText) findViewById(R.id.txtTenKT);
        btnXacNhanKhenThuong= (Button) findViewById(R.id.btnXacNhanKhenThuong);
        btnXacNhanKiLuat= (Button) findViewById(R.id.btnXacNhanKiLuat);
        lvKhenThuong= (ListView) findViewById(R.id.lvKhenThuong);
        lvKiLuat= (ListView) findViewById(R.id.lvKiLuat);
        dsDoiTuong=new ArrayList<>();
        adapter=new KhenThuongAdapter(ManHinh6Activity.this,R.layout.itemkt,dsDoiTuong);
        lvKhenThuong.setAdapter(adapter);
        dsKiluat=new ArrayList<>();
        kiLuatAdapter=new KiLuatAdapter(ManHinh6Activity.this,R.layout.itemkl,dsKiluat);
        lvKiLuat.setAdapter(kiLuatAdapter);
    }
}
