package com.example.asus.asignment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.app.Activity;
import android.view.Window;
public class MainActivity extends Activity {

    Button btnThemLop,btnXem,btnQuanLy,btnSapXep,btnThongTin,btnDanhSachSinhVien;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        setTitle("Bài Assignment");
        addControls();
        addEvents();
    }

    private void addEvents() {
        btnThemLop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XuLyThemLop();


            }
        });
        btnXem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XuLyXemLop();
            }
        });
        btnQuanLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XuLyQuanLySinhVien();
            }
        });
        btnSapXep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ManHinh5Activity.class);
                startActivity(intent);
            }
        });
        btnThongTin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ManHinh6Activity.class);
                startActivity(intent);
            }
        });
        btnDanhSachSinhVien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,ListSinhVienActivity.class);
                startActivity(intent);
            }
        });
    }

    private void XuLyQuanLySinhVien() {
        Intent intent=new Intent(MainActivity.this,Student_ManagementActivity.class);
        startActivity(intent);
    }

    private void XuLyThemLop() {
        Intent intent=new Intent(MainActivity.this,DialogActivity.class);
        startActivity(intent);
    }

    private void XuLyXemLop() {
        Intent intent=new Intent(MainActivity.this,ListClassActivity.class);
        startActivity(intent);
    }

    private void addControls() {
        btnThemLop= (Button) findViewById(R.id.btnThemLop);
        btnXem= (Button) findViewById(R.id.btnXem);
        btnQuanLy= (Button) findViewById(R.id.btnQuanLy);
        btnSapXep= (Button) findViewById(R.id.btnSapXep);
        btnThongTin= (Button) findViewById(R.id.btnThongTin);
        btnDanhSachSinhVien= (Button) findViewById(R.id.btnDanhSachSinhVien);

    }



}
